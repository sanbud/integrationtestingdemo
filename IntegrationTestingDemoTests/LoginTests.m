//
//  IntegrationTestingDemoTests.m
//  IntegrationTestingDemoTests
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "AuthenticationScenario.h"
#import "TestingAppDelegate.h"
#import "LoginVC.h"

//=============================================================================

@interface TestingClient : NSObject
<
ScenarioDelegate,
AuthenticationProtocol
>

@end

//=============================================================================

@implementation TestingClient

/// Handle User is selected.
- (void)handleUser {
    
}

/// Handle Guest is selected.
- (void)handleGuest {
    
}

/// Handle To Dashboard is selected.
- (void)handleToDashboard {
    
}

- (void)didStart:(Scenario *)scenario {
    
}

- (void)didFinish:(Scenario *)scenario {
    
}

@end

//=============================================================================

@interface IntegrationTestingDemoTests : XCTestCase

@property AuthenticationScenario * authenticationScenario;

@end

//=============================================================================

@interface LoginVC ()

- (IBAction)onUser:(id)sender;
- (IBAction)onGuest:(id)sender;
- (IBAction)toDashboard:(id)sender;

@end

//=============================================================================

@implementation IntegrationTestingDemoTests


- (void)setUp {
    [super setUp];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)test_onUserTapped {
    
    TestingClient * integrationClient = TestingClient.new;
    
    self.authenticationScenario =
    [AuthenticationScenario authenticationScenarioWithDelegate:integrationClient
                                                  authDelegate:integrationClient];
    self.authenticationScenario.isTest = YES;
    
    [self.authenticationScenario start];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    window.rootViewController = (UIViewController *)self.authenticationScenario.rootNvc;
    
    [window makeKeyAndVisible];
    
    TestingAppDelegate *appDelegate = (TestingAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.window = window;
    
    LoginVC * loginVC = (LoginVC *)self.authenticationScenario.rootNvc.topViewController;
    
    [loginVC onUser:nil];
    
    UIViewController * vc = self.authenticationScenario.rootNvc.topViewController;
    
    XCTAssert([vc.title isEqualToString:@"User"]);
    
}

//=============================================================================

- (void)test_onGuestTapped {
    
    TestingClient * integrationClient = TestingClient.new;
    
    self.authenticationScenario =
    [AuthenticationScenario authenticationScenarioWithDelegate:integrationClient
                                                  authDelegate:integrationClient];
    self.authenticationScenario.isTest = YES;
    
    [self.authenticationScenario start];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    window.rootViewController = (UIViewController *)self.authenticationScenario.rootNvc;
    
    [window makeKeyAndVisible];
    
    TestingAppDelegate *appDelegate = (TestingAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.window = window;
    
    LoginVC * loginVC = (LoginVC *)self.authenticationScenario.rootNvc.topViewController;
    
    [loginVC onGuest:nil];
    
    UIViewController * vc = self.authenticationScenario.rootNvc.topViewController;
    
    XCTAssert([vc.title isEqualToString:@"Guest"]);
}

//=============================================================================

- (void)test_onUserTapped_throughMockedClient {
    
    TestingClient * integrationClient = TestingClient.new;
    
    self.authenticationScenario =
    [AuthenticationScenario authenticationScenarioWithDelegate:integrationClient
                                                  authDelegate:integrationClient];
    self.authenticationScenario.isTest = YES;
    
    [self.authenticationScenario start];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    window.rootViewController = (UIViewController *)self.authenticationScenario.rootNvc;
    
    [window makeKeyAndVisible];
    
    TestingAppDelegate *appDelegate = (TestingAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.window = window;
    
    LoginVC * loginVC = (LoginVC *)self.authenticationScenario.rootNvc.topViewController;
    
    id mockedClient = OCMPartialMock(integrationClient);
    
    OCMStub([mockedClient didFinish:OCMArg.any]);
    OCMStub([mockedClient handleUser]);
    
    [loginVC onUser:nil];
    
    OCMVerify([mockedClient handleUser]);
}

//=============================================================================

- (void)test_onGuestTapped_throughMockedClient {
    
    TestingClient * integrationClient = TestingClient.new;
    
    self.authenticationScenario =
    [AuthenticationScenario authenticationScenarioWithDelegate:integrationClient
                                                  authDelegate:integrationClient];
    self.authenticationScenario.isTest = YES;
    
    [self.authenticationScenario start];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    window.rootViewController = (UIViewController *)self.authenticationScenario.rootNvc;
    
    [window makeKeyAndVisible];
    
    TestingAppDelegate *appDelegate = (TestingAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.window = window;
    
    LoginVC * loginVC = (LoginVC *)self.authenticationScenario.rootNvc.topViewController;
    
    [loginVC onGuest:nil];
    
    UIViewController * vc = self.authenticationScenario.rootNvc.topViewController;
    
    XCTAssert([vc.title isEqualToString:@"Guest"]);
}


@end
