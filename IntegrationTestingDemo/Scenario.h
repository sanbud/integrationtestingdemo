//
//  Scenario.h
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class Scenario;

//=============================================================================

/// The delegate to notify about scenario stages.
@protocol ScenarioDelegate <NSObject>

/** Notify the delegate that just started.
 @param scenario The started scenario.
 */
- (void)didStart:(Scenario *)scenario;

/** Notify the delegate that just finished.
 @param scenario The finished scenario.
 */
- (void)didFinish:(Scenario *)scenario;

@end

//=============================================================================

@interface Scenario : NSObject

@property UINavigationController * rootNvc;
/**
 * Initialaze scenario with scenario delegate.
 * @param delegate The delegate to notify about scenario flow.
 * @result The scenario instance.
 */
- (instancetype)initWithDelegate:(id<ScenarioDelegate>)delegate;

/// The delegate to notify about the scenario state was changed.
@property id<ScenarioDelegate> delegate;

/// Start scenario.
- (void)start;

/// Flag for test environment.
@property BOOL isTest;

@end

//=============================================================================

NS_ASSUME_NONNULL_END
