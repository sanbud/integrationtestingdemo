//
//  LoginVC.m
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()

@property id<LoginDelegate> loginDelegate;

@end

//=============================================================================

@implementation LoginVC

+ (LoginVC *)loginVCWithDelegate:(id<LoginDelegate>)delegate {
    
    return [LoginVC.alloc initWithLoginDelegate:delegate];
}

- (instancetype)initWithLoginDelegate:(id<LoginDelegate>)loginDelegate {
    
    self = [super initWithNibName:NSStringFromClass(self.class)
                           bundle:nil];
    
    if (self) {
        
        _loginDelegate  = loginDelegate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - IB Actions

- (IBAction)onUser:(id)sender {
    [self.loginDelegate didTapAsUser];
}

- (IBAction)onGuest:(id)sender {
    [self.loginDelegate didTapAsGuest];
}

- (IBAction)toDashboard:(id)sender {
    [self.loginDelegate didTapToDashboard];
}

@end
