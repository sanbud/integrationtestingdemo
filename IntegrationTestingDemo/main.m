//
//  main.m
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

/// Class name for Unit test AppDelegate.
static NSString *const kUnitTestAppDelegate = @"TestingAppDelegate";

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        /// Here is we try use specific TestingAppDelegate for testing.
        Class appDelegateClass = NSClassFromString(kUnitTestAppDelegate) ?
        NSClassFromString(kUnitTestAppDelegate) : AppDelegate.class;

        return UIApplicationMain(argc, argv, nil, NSStringFromClass(appDelegateClass));
    }
}
