//
//  AuthenticationScenario.h
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import "Scenario.h"

NS_ASSUME_NONNULL_BEGIN

//=============================================================================

@protocol AuthenticationProtocol

/// Handle User is selected.
- (void)handleUser;

/// Handle Guest is selected.
- (void)handleGuest;

/// Handle To Dashboard is selected.
- (void)handleToDashboard;

@end

//=============================================================================

@class LoginVC;

//=============================================================================

@interface AuthenticationScenario : Scenario

/// Login UI.
@property LoginVC * loginVC;

/**
 Creation method
 @param delegate The scenario lifecycle responder.
 @param authDelegate The external responder.
 @return AuthenticationScenario instance.
 */
+ (AuthenticationScenario *)authenticationScenarioWithDelegate:(id<ScenarioDelegate>)delegate
                                                  authDelegate:(id<AuthenticationProtocol>)authDelegate;

@end

//=============================================================================

NS_ASSUME_NONNULL_END
