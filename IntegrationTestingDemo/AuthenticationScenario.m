//
//  AuthenticationScenario.m
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import "AuthenticationScenario.h"
#import "LoginVC.h"

//=============================================================================

@interface AuthenticationScenario ()
<
LoginDelegate
>

@property id<AuthenticationProtocol> logicDelegate;

@end

//=============================================================================

@implementation AuthenticationScenario

+ (AuthenticationScenario *)authenticationScenarioWithDelegate:(id<ScenarioDelegate>)delegate
                                                  authDelegate:(id<AuthenticationProtocol>)authDelegate; {
    
    AuthenticationScenario *as = [AuthenticationScenario.alloc initWithDelegate:delegate];
    
    as.logicDelegate = authDelegate;
    
    return as;
}

- (void)start {
    
    self.loginVC = [LoginVC loginVCWithDelegate:self];
    self.rootNvc = [[UINavigationController alloc] initWithRootViewController:self.loginVC];
}

//=============================================================================

#pragma mark - <LoginDelegate>

- (void)didTapAsGuest {
    
    UIViewController * vc = UIViewController.new;
    
    vc.title = @"Guest";
    
    [self.logicDelegate handleGuest];

    [self.rootNvc pushViewController:vc animated:!self.isTest];
    
    [self.delegate didFinish:self];
}

- (void)didTapAsUser {

    UIViewController * vc = UIViewController.new;
    
    vc.title = @"User";
    
    [self.logicDelegate handleUser];
    
    [self.rootNvc pushViewController:vc animated:!self.isTest];
    
    [self.delegate didFinish:self];
}

- (void)didTapToDashboard {
    
    UIViewController * vc = UIViewController.new;
    
    vc.title = @"To Dashboard";
    
    [self.logicDelegate handleToDashboard];
    
    [self.rootNvc pushViewController:vc animated:!self.isTest];
    
    [self.delegate didFinish:self];
}

@end
