//
//  TestingAppDelegate.h
//  IntegrationTestingDemo
//
//  Created by Aleksandr Budaiev on 8/14/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestingAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

NS_ASSUME_NONNULL_END
