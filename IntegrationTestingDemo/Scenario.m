//
//  Scenario.m
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import "Scenario.h"

@implementation Scenario

- (instancetype)initWithDelegate:(id<ScenarioDelegate>)delegate {
    
    self = [super init];
    
    if (self) {
        
        _rootNvc = UINavigationController.new;
        _delegate = delegate;
    }
    
    return self;
}

- (void)start {
    
}



@end
