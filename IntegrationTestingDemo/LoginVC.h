//
//  LoginVC.h
//  IntegrationTestingDemo
//
//  Created by Aleksandr Bydaiev on 5/29/19.
//  Copyright © 2019 com.dntl.org. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//=============================================================================

@protocol LoginDelegate <NSObject>

/// User is pressed.
- (void)didTapAsUser;

/// Guest is pressed.
- (void)didTapAsGuest;

/// To dashboard is pressed.
- (void)didTapToDashboard;

@end

//=============================================================================

@interface LoginVC : UIViewController

/** Creation method
 @param delegate The external logic handler.
 @return LoginVC screen instance.
 */

+ (LoginVC *)loginVCWithDelegate:(id<LoginDelegate>)delegate;

@end

//=============================================================================

NS_ASSUME_NONNULL_END
